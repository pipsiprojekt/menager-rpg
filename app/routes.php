Auth::routes();
Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/contacts', 'PagesController@contacts');

Route::resource('posts', 'PostsController');

Route::get('/dashboard', 'DashboardController@index');
Route::post('/dashboard', 'DashboardController@update_avatar');

Route::get('/dices', 'DicesController@index');

Lang::setLocale('pl');