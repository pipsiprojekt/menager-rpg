<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hero;
use Auth;
use Image;

class HeroesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $heroes = Hero::orderBy('created_at', 'desc')->paginate(5);
        return view('heroes.index')->with('heroes', $heroes);
    }

    
    public function create()
    {
        return view('heroes.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $hero = new Hero;
        $hero->name = $request->input('name');
        $hero->rase = $request->input('rase');
        $hero->proffesion = $request->input('proffesion');
        $hero->sex = $request->input('sex');
        $hero->WS = $request->input('WS');
        $hero->BS = $request->input('BS');
        $hero->T = $request->input('T');
        $hero->S = $request->input('S');
        $hero->Ag = $request->input('Ag');
        $hero->Int = $request->input('Int');
        $hero->WP = $request->input('WP');
        $hero->Fel = $request->input('Fel');
        //$hero->portrait = $request->file('portrait');
        $hero->user_id = auth()->user()->id;
        $hero->save();
        return redirect('/heroes')->with('success', 'Stworzono Postać');
    }

    
    public function show($id)
    {
        $hero = Hero::find($id);
        return view('heroes.show')->with('hero', $hero);
    }


    public function edit($id)
    {
        $hero = Hero::find($id);

        if(auth()->user()->id !==$hero->user_id){
            return redirect('/heroes')->with('error', 'TAK NIE WOLNO!!!');

        }
        return view('heroes.edit')->with('hero', $hero);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $hero = Hero::find($id);
        $hero->name = $request->input('name');
        $hero->rase = $request->input('rase');
        $hero->proffesion = $request->input('proffesion');
        $hero->sex = $request->input('sex');
        $hero->WS = $request->input('WS');
        $hero->BS = $request->input('BS');
        $hero->T = $request->input('T');
        $hero->S = $request->input('S');
        $hero->Ag = $request->input('Ag');
        $hero->Int = $request->input('Int');
        $hero->WP = $request->input('WP');
        $hero->Fel = $request->input('Fel');
        //$hero->portrait = $request->input('portrait');
        $hero->save();
        return redirect('/heroes')->with('success', 'Edytowano Postać');
    }

    public function destroy($id)
    {
        $hero = Hero::find($id);
        if(auth()->user()->id !==$hero->user_id){
            return redirect('/heroes')->with('error', 'TAK NIE WOLNO!!!');

        }
        $hero->delete();
        return redirect('/heroes')->with('success', 'Postać usunięta');
    }


}