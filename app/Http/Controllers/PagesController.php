<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PagesController extends Controller
{
    public function index(){
    	$title = 'Menadżer RPG';
    	return view('pages.index', compact('title'));
    }

    public function about(){
    	$title = 'Informacje';
    	return view('pages.about', compact('title'));
    }

    public function contacts(){
    	$data = array(
    		'title' => 'Kontakt',
    		'contacts' => ['E-mail: menagerrpg@gmail.com', 'tel: 123456798']
    	);
    	return view('pages.contacts')->with($data);
    }

}
