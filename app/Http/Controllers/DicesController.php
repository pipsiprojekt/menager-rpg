<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DicesController extends Controller
{
    public function index(){
    	$title = 'Rzuć kośćmi';
    	return view('pages.dices', compact('title'));
    }
    
    function roll($side, $number){
        return rand(1, $side)*$number;
    } 
}
