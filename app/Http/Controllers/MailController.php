<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class MailController extends Controller
{


	public function send()
	{
		Mail::send(new SendMail);
	}

	public function email()
	{
		return view('emails/email');
	}

}
