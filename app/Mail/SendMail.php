<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\request;
use App\user;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;



    public function __construct()
    {
        //
    }

    public function build(request $request)
    {
        $user = User::find(1);
        return $this->view('emails/email-welcome', ['name'=>$user->name])->to($user->email);
    }
}