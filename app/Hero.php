<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hero extends Model
{
    // Nazwa tabeli
    protected $table = 'heroes';
    //Klucz
    public $primaryKey = 'id';
    //Timestamps
    public $timestamps = true;

    public function user(){
    	return $this->belongsTo('App\User');
    }
}
