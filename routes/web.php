<?php

/*
Route::get('/', function () {
    return view('welcome');
});
*/

/*
Route::get('/home', 'HomeController@index')->name('home');
*/

Auth::routes();
Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/contacts', 'PagesController@contacts');

Route::resource('posts', 'PostsController');

Route::get('/dashboard', 'DashboardController@index');
Route::post('/dashboard', 'DashboardController@update_avatar');

Route::get('/dices', 'DicesController@index');

Route::resource('heroes', 'HeroesController');

Auth::routes(['verify' => true]);

Route::get('send', 'MailController@send');
//Route::get('email', 'MailController@email');

Lang::setLocale(''); //automatyczne wybieranie lokacji
//Lang::setLocale('pl'); 
//Lang::setLocale('en'); 
