<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="author" content="Patryk Reszczyński Szymon Grabarkiewicz">
    <meta name="description" content="Projekt Patryka Reszczyńskiego i Szymona Grabarkiewicza; tworzenie newsów, tworzenie bohaterów, rzuty kośćmi">
    <meta name="keywords" content="PHP, Laravel, Patryk Reszczyński, Szymon Grabarkiewicz, RPG, Bohater">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Menadżer RPG') }}</title>

    <script type="text/javascript" rel="script" src="{{asset('js/app.js')}}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
<style>
            html, body {
                background-color: #f8fafe;
                color: #000000;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #000000;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .buttons > a {
                height: 50 px;
                width: 5 px;
                color: #000000;
                padding: 0 25px;
                font-size: 15px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                border: 1px solid black;
                border-radius: 15px;
                border-color: #888888;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .post {
                margin: 10px;
                border: 1px solid black;
                border-radius: 10px;
                border-color: #888888;
            }
            .page {
                background-color: #eeeeee;
                margin: 10px;
                border: 1px solid black;
                border-radius: 10px;
                border-color: #888888;
            }
        </style>
    
</head>
<body>
    <div id="app">
            @include('inc.navbar')
            <div class="container">
            @include('inc.messages')
            
            @yield('content')
            </div>
    </div>

<!-- Scripts -->
    
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'article-ckeditor' );
    </script>
          
</body>
</html>




