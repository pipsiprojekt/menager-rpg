@extends('layouts.app')

@section('content')
</br>
<div class="card bg-light mb-3">
       <div class="card-header"><h1>{{$title}}</h1></div>
        @if(count($contacts)>0)
	        <ul class="list-group">
	        	@foreach($contacts as $contact)
	        		<li class="list-group-item">{{$contact}}</li>
	        	@endforeach
	        </ul>
	    </div>
        @endif
@endsection