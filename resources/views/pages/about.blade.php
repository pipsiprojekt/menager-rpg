@extends('layouts.app')

@section('content')
</br>
<div class="card bg-light mb-3">
    <div class="card-header"><h1>{{$title}}</h1></div>
    <div class="card-body">
    	<p>Strona zbudowana w celu ułatwienia tworzenia bohaterów do gier RPG</p>
    </div>
</div>
@endsection