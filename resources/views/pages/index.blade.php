@extends('layouts.app')

@section('content')
</br>

        <div class="jumbotron">
  <h1 class="display-4">{{$title}}</h1>
  <p class="lead">Tutaj stworzysz swojego bohatera do RPG Warhammer 2 edycja</p>
  <hr class="my-4">
  
  <p class="lead">
  	@guest
  	<p>Zaloguj się lub zarejestruj jeśli jeszcze nie masz konta</p>
	        <a class="btn btn-primary btn-lg" href="{{ route('login') }}" role="button">Zaloguj</a>
	    @if (Route::has('register'))
	        <a class="btn btn-primary btn-lg" href="{{ route('register') }}" role="button">Zarejestruj</a>
	    @endif
    @else
    <h2>Witaj ponownie</h2>
    <p>Możesz odwiedzić swój profil</p>
    <a class="btn btn-primary btn-lg" href="/dashboard" role="button">Mój profil</a>

    
    @endguest

    
  </p>
</div>
@endsection
