@extends('layouts.app')

@section('content')
</br>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="card-title"><h1>{{ Auth::user()->name }}</h1></div>
                    <img src="/uploads/avatars/{{Auth::user()->avatar}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
                    <form enctype="multipart/form-data" action="/dashboard" method="POST">
                    <h4>Zmień avatar</h4>
                    <input type="file" name="avatar">
                    <input type="hidden" name="_token" value="{{ csrf_token()}}">
                    <input type="submit" class="pull-right btn btn-sm btn-primary">
                    </form>
                </div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                    <div class="card-header">Posiadane postacie</div>

                        @if(count($heroes)>0)
                            <table class="table table striped">
                                <tr>
                                    <th>Imię</th>
                                    <th>Rasa</th>
                                    <th>Profesja</th>
                                    <th></th>
                                </tr>
                                @foreach($heroes as $hero)
                                <tr>
                                    <th>{{$hero->name}}</th>
                                    <th>{{$hero->rase}}</th>
                                    <th>{{$hero->proffesion}}</th>
                                    <th><a href="/heroes/{{$hero->id}}/edit" class="btn btn-primary">Edytuj</a></th>
                                    
                                </tr>
                                @endforeach
                            </table>
                            @else
                            <p>Nie masz żadnych postaci</p>
                        @endif
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection
