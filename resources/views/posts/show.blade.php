@extends('layouts.app')

@section('content')

</br>
<a href="/posts" class="btn btn-primary">Powrót</a>
</br>
	<div class="card bg-light mb-3">
		<div classs="card-header"><h1>{{$post->title}}</h1></div>
		<div class="card-body">{!!$post->body!!}</div>
		<div class="card-footer bg-light"><small class="text-muted">Opublikowano {{$post->created_at}} przez: {{$post->user->name}}</small></div>
	</div>
			<hr>
			@if(!Auth::guest())
			@if(Auth::user()->id == $post->user_id)
			<a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edytuj</a>

	{!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
		{{Form::hidden('_method', 'DELETE')}}
		{{Form::submit('Usuń',  ['class' => 'btn btn-danger'])}}
	{!!Form::close()!!}

	@endif
	@endif
@endsection