@extends('layouts.app')

@section('content')
</br>

<div class="card mb-3">
    <div class="card-header"><h1>Newsy</h1></div>
	
	@if(count($posts)>0)
		@foreach($posts as $post)
		<div class="card bg-light mb-3">
			<div class="card-header"><h2><a href="/posts/{{$post->id}}">{{$post->title}}</a></h2></div>
				<div class="card-body">
					<p>{!!$post->body!!}</p>
					<div class="card-footer bg-light"><small class="text-muted">Opublikowano: {{$post->created_at}} przez: {{$post->user->name}}</small></div>
				</div>
		</div>
		@endforeach
		{{$posts->links()}}

	@else
		<p>No posts found</p>
	@endif
</div>
@endsection