@extends('layouts.app')

@section('content')
</br>
<div class="card bg-light mb-3">
	<div classs="card-header"><h1>Napisz Newsa</h1></div>
	{!! Form::open(['action'=> 'PostsController@store', 'method' => 'POST']) !!}
		<div class="card-title">
			{{Form::label('title', 'Tytuł')}}
			{{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Tytuł...'])}}
		</div>
		<div class="card-body">
			{{Form::label('body', 'Treść')}}
			{{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Treść...'])}}
		</div>
		{{Form::submit('Opublikuj', ['class' => 'btn btn-primary'])}}
	{!! Form::close() !!}
</div>
@endsection