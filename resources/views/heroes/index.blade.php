@extends('layouts.app')

@section('content')
</br>

<div class="card mb-3">
    <div class="card-header"><h1>Stwórzone postacie</h1></div>
	@if(count($heroes)>0)
		@foreach($heroes as $hero)
		<div class="card bg-light mb-3">
				<div class="card-header"><h2><a href="/heroes/{{$hero->id}}">{{$hero->name}}</a></h2></div>
				<div class="card-body">
					<img src="/uploads/portraits/{{$hero->portrait}}" style="width:150px; height:150px; float:left; margin-right:25px; margin-bottom: 25px;">
					<table class="table">
						<thead class="thead-light">
					  	<tr>
					      <th scope="col">Rasa</th>
					      <th scope="col">Płeć</th>
					      <th scope="col">Profesja</th>
					    </tr>
					</thead>
					    <tbody>
					    <tr>
					      <td>{{$hero->rase}}</td>
					      <td>{{$hero->sex}}</td>
					      <td>{{$hero->proffesion}}</td>
					    </tr>
					  </tbody>
					</table>
					<table class="table">
					  <thead class="thead-light">
					  	<tr>
					      <th scope="col">WW</th>
					      <th scope="col">US</th>
					      <th scope="col">S</th>
					      <th scope="col">Wt</th>
					      <th scope="col">Zr</th>
					      <th scope="col">Int</th>
					      <th scope="col">SW</th>
					      <th scope="col">Ogd</th>
					    </tr>
					</thead>
					  <tbody>
					    <tr>
					      <td>{{$hero->WS}}</td>
					      <td>{{$hero->BS}}</td>
					      <td>{{$hero->S}}</td>
					      <td>{{$hero->T}}</td>
					      <td>{{$hero->Ag}}</td>
					      <td>{{$hero->Int}}</td>
					      <td>{{$hero->WP}}</td>
					      <td>{{$hero->Fel}}</td>
					    </tr>
					  </tbody>
					</table>
					<div class="card-footer bg-light"><small class="text-muted">Stworzono: {{$hero->created_at}} Przez: {{$hero->user->name}}</small></div>
				</div>
		</div>
		@endforeach
		{{$heroes->links()}}
	@else
		<p>Nie ma żadnych postaci</p>
	@endif
</div>

		
@endsection