@extends('layouts.app')

@section('content')
</br>

<div class="card bg-light mb-3">
    <div class="card-header"><h1>Stwórz postać</h1></div>
    <div class="card-body">
    	{!! Form::open(['action'=> 'HeroesController@store', 'method' => 'POST']) !!}
		<div class="card-title">
			{{Form::label('name', 'Imię')}}
			{{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Imię...'])}}
		</div>
		<div class="card-body">
			<table class="table">
						<thead class="thead-light">
					  	<tr>
					      <th scope="col">{{Form::label('rase', 'Rasa')}}</th>
					      <th scope="col">{{Form::label('sex', 'Płeć')}}</th>
					      <th scope="col">{{Form::label('proffesion', 'Profesja')}}</th>
					    </tr>
					</thead>
					    <tbody>
					    <tr>
					      <td>{{Form::select('rase', array('Człowiek'=>'Człowiek', 'Elf'=>'Elf', 'Krasnolud'=>'Krasnolud', 'Niziołek'=>'Niziołek'), 'Człowiek')}}</td>
					      <td>{{Form::select('sex', array('Mężczyzna'=>'Mężczyzna', 'Kobieta'=>'Kobieta'), 'Mężczyzna')}}</td>
					      <td>{{Form::select('proffesion', array('Wojownik'=>'Wojownik', 'Łucznik'=>'Łucznik', 'Mag'=>'Mag', 'Łotrzyk'=>'Łotrzyk'), 'Wojownik')}}</td>
					    </tr>
					  </tbody>

					  <thead class="thead-light">
					  	<tr>
					      <th scope="col">{{Form::label('WS', 'WW')}}</th>
					      <th scope="col">{{Form::label('BS', 'US')}}</th>
					      <th scope="col">{{Form::label('S', 'S')}}</th>
					      <th scope="col">{{Form::label('T', 'Wt')}}</th>
					      <th scope="col">{{Form::label('Ag', 'Zr')}}</th>
					      <th scope="col">{{Form::label('Int', 'Int')}}</th>
					      <th scope="col">{{Form::label('WP', 'SW')}}</th>
					      <th scope="col">{{Form::label('Fel', 'Ogd')}}</th>
					    </tr>
					</thead>
					  <tbody>
					    <tr>
					      <td>{{Form::number('WS', '1', ['min'=>1,'max'=>100, 'placeholder' => '1'])}}</td>
					      <td>{{Form::number('BS', '1', ['min'=>1,'max'=>100, 'placeholder' => '1'])}}</td>
					      <td>{{Form::number('S', '1', ['min'=>1,'max'=>100, 'placeholder' => '1'])}}</td>
					      <td>{{Form::number('T', '1', ['min'=>1,'max'=>100, 'placeholder' => '1'])}}</td>
					      <td>{{Form::number('Ag', '1', ['min'=>1,'max'=>100, 'placeholder' => '1'])}}</td>
					      <td>{{Form::number('Int', '1', ['min'=>1,'max'=>100, 'placeholder' => '1'])}}</td>
					      <td>{{Form::number('WP', '1', ['min'=>1,'max'=>100, 'placeholder' => '1'])}}</td>
					      <td>{{Form::number('Fel', '1', ['min'=>1,'max'=>100, 'placeholder' => '1'])}}</td>
					    </tr>
					  </tbody>
					</table>
			
		</div>
		{{Form::submit('Stwórz', ['class' => 'btn btn-primary'])}}
	{!! Form::close() !!}

    </div>
</div>
		
@endsection