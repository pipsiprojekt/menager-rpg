@extends('layouts.app')

@section('content')
<a href="/heroes/{{$hero->id}}" class="btn btn-primary">Cofnij</a>
<div class="card bg-light mb-3">
    <div class="card-header"><h1>Edytuj postać</h1></div>
    <div class="card-body">
    	{!! Form::open(['action'=> ['HeroesController@update', $hero->id], 'method' => 'POST']) !!}
		<div class="card-title">
			{{Form::label('name', 'Imię')}}
			{{Form::text('name', $hero->name, ['class' => 'form-control', 'placeholder' => 'Imię...'])}}
		</div>
		<div class="card-body">
			<img src="/uploads/portraits/{{$hero->portrait}}" style="width:150px; height:150px; float:left; margin-right:25px; margin-bottom: 25px;">
			<table class="table">
						<thead class="thead-light">
					  	<tr>
					      <th scope="col">{{Form::label('rase', 'Rasa')}}</th>
					      <th scope="col">{{Form::label('sex', 'Płeć')}}</th>
					      <th scope="col">{{Form::label('proffesion', 'Profesja')}}</th>
					    </tr>
					</thead>
					    <tbody>
					    <tr>
					      <td>{{Form::select('rase', array('Człowiek'=>'Człowiek', 'Elf'=>'Elf', 'Krasnolud'=>'Krasnolud', 'Niziołek'=>'Niziołek'), $hero->rase)}}</td>
					      <td>{{Form::select('sex', array('Mężczyzna'=>'Mężczyzna', 'Kobieta'=>'Kobieta'), $hero->sex)}}</td>
					      <td>{{Form::select('proffesion', array('Wojownik'=>'Wojownik', 'Łucznik'=>'Łucznik', 'Mag'=>'Mag', 'Łotrzyk'=>'Łotrzyk'), $hero->proffesion)}}</td>
					    </tr>
					  </tbody>

					  <thead class="thead-light">
					  	<tr>
					      <th scope="col">{{Form::label('WS', 'WW')}}</th>
					      <th scope="col">{{Form::label('BS', 'US')}}</th>
					      <th scope="col">{{Form::label('S', 'S')}}</th>
					      <th scope="col">{{Form::label('T', 'Wt')}}</th>
					      <th scope="col">{{Form::label('Ag', 'Zr')}}</th>
					      <th scope="col">{{Form::label('Int', 'Int')}}</th>
					      <th scope="col">{{Form::label('WP', 'SW')}}</th>
					      <th scope="col">{{Form::label('Fel', 'Ogd')}}</th>
					    </tr>
					</thead>
					  <tbody>
					    <tr>
					      <td>{{Form::number('WS', $hero->WS, ['min'=>1,'max'=>100, 'placeholder' => $hero->WS])}}</td>
					      <td>{{Form::number('BS', $hero->BS, ['min'=>1,'max'=>100, 'placeholder' => $hero->BS])}}</td>
					      <td>{{Form::number('S', $hero->S, ['min'=>1,'max'=>100, 'placeholder' => $hero->S])}}</td>
					      <td>{{Form::number('T', $hero->T, ['min'=>1,'max'=>100, 'placeholder' => $hero->T])}}</td>
					      <td>{{Form::number('Ag', $hero->Ag, ['min'=>1,'max'=>100, 'placeholder' => $hero->Ag])}}</td>
					      <td>{{Form::number('Int', $hero->Int, ['min'=>1,'max'=>100, 'placeholder' => $hero->Int])}}</td>
					      <td>{{Form::number('WP', $hero->WP, ['min'=>1,'max'=>100, 'placeholder' => $hero->WP])}}</td>
					      <td>{{Form::number('Fel', $hero->Fel, ['min'=>1,'max'=>100, 'placeholder' => $hero->Fel])}}</td>
					    </tr>
					  </tbody>
					</table>
			


		</div>
		{{Form::hidden('_method', 'PUT')}}
		{{Form::submit('Zapisz', ['class' => 'btn btn-primary'])}}
	{!! Form::close() !!}
<button type="button" class="btn btn-danger float-right" data-toggle="modal" data-target="#exampleModal">
  Usuń
</button>



<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Czy na pewno usunąć?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<h2>Usunięcie postaci będzie nieodwracalne</h2>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cofnij</button>
        {!!Form::open(['action' =>['HeroesController@destroy', $hero->id], 'method' => 'POST', 'class' => 'float-right'])!!}
						{{Form::hidden('_method', 'DELETE')}}
						{{Form::submit('Usuń', ['class'=>'btn btn-danger'])}}
					{!!Form::close()!!}
      </div>
    </div>
  </div>
</div>

    </div>
</div>
		
@endsection