<?php 
return array (
  'failed' => 'Wrong data has been entered.',
  'throttle' => 'Too many login attempts. Please try again in a few :seconds seconds.',
);