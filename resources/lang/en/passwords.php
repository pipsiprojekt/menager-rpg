<?php 
return array (
  'password' => 'The password must be at least 8 characters long.',
  'reset' => 'Your password has been reset',
  'sent' => 'We have sent you a password reset link to the e-mail address!',
  'token' => 'An incorrect token has been entered.',
  'user' => 'We can not find a user with the given email address.',
);