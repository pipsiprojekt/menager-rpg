<?php 
return array (
  'accepted' => ' :attribute musi zostać zaakceptowany.',
  'active_url' => ' :attribute nie jest prawidłowym adresem URL.',
  'after' => ' :attribute musi być datą po :date.',
  'after_or_equal' => ' :attribute musi być datą po lub równą :date.',
  'alpha' => ' :attribute może zawierać tylko litery.',
  'alpha_dash' => ' :attribute może zawierać tylko litery, cyfry, myślniki i podkreślniki.',
  'alpha_num' => ' :attribute może zawierać tylko litery i cyfry.',
  'array' => ' :attribute musi być tablicą.',
  'before' => ' :attribute musi być datą sprzed :date.',
  'before_or_equal' => ' :attribute musi być datą przed lub równą :date.',
  'between' => 
  array (
    'numeric' => 'Wartość :attribute musi zawierać się między :min a :max.',
    'file' => 'Wartość :attribute musi zawierać się między :min a :max kilobajtów.',
    'string' => 'Wartość :attribute musi zawierać się między znakami :min i :max.',
    'array' => ' :attribute musi mieć pomiędzy elementami :min i :max.',
  ),
  'boolean' => 'Pole :attribute musi być prawdziwe lub fałszywe.',
  'confirmed' => 'Potwierdzenie :attribute nie pasuje.',
  'date' => ' :attribute nie jest prawidłową datą.',
  'date_equals' => ' :attribute musi być datą równą :date.',
  'date_format' => ' :attribute nie pasuje do formatu :format.',
  'different' => ' :attribute i :other muszą być różne.',
  'digits' => ' :attribute musi być cyfrą :digits.',
  'digits_between' => 'Wartość :attribute musi zawierać się między cyframi :min i :max.',
  'dimensions' => ' :attribute ma nieprawidłowe wymiary obrazu.',
  'distinct' => 'Pole :attribute ma zduplikowaną wartość.',
  'email' => ' :attribute musi być prawidłowym adresem e-mail.',
  'exists' => 'Wybrany :attribute jest nieprawidłowy.',
  'file' => ' :attribute musi być plikiem.',
  'filled' => 'Pole :attribute musi mieć wartość.',
  'gt' => 
  array (
    'numeric' => 'Wartość :attribute musi być większa niż :value.',
    'file' => 'Wartość :attribute musi być większa niż :value kilobajty.',
    'string' => 'Wartość :attribute musi być większa niż :value znaków.',
    'array' => ' :attribute musi mieć więcej niż 2 przedmioty.',
  ),
  'gte' => 
  array (
    'numeric' => 'Wartość :attribute musi być większa lub równa :value.',
    'file' => 'Wartość :attribute musi być większa lub równa :value kilobajtów.',
    'string' => ' :attribute musi być większy lub równy :value znaków.',
    'array' => ' :attribute musi mieć przedmioty :value lub więcej.',
  ),
  'image' => ' :attribute musi być obrazem.',
  'in' => 'Wybrany :attribute jest nieprawidłowy.',
  'in_array' => 'Pole :attribute nie istnieje w :other.',
  'integer' => ' :attribute musi być liczbą całkowitą.',
  'ip' => ' :attribute musi być prawidłowym adresem IP.',
  'ipv4' => ' :attribute musi być prawidłowym adresem IPv4.',
  'ipv6' => ' :attribute musi być prawidłowym adresem IPv6.',
  'json' => ' :attribute musi być prawidłowym ciągiem JSON.',
  'lt' => 
  array (
    'numeric' => 'Wartość :attribute musi być mniejsza niż :value.',
    'file' => 'Wartość :attribute musi być mniejsza niż :value kilobajty.',
    'string' => 'Wartość :attribute musi być mniejsza niż 2 znaki.',
    'array' => ' :attribute musi mieć mniej niż :value przedmiotów.',
  ),
  'lte' => 
  array (
    'numeric' => 'Wartość :attribute musi być mniejsza lub równa :value.',
    'file' => 'Wartość :attribute musi być mniejsza lub równa :value kilobajtów.',
    'string' => ' :attribute musi być mniejszy lub równy :value znaków.',
    'array' => ' :attribute nie może mieć więcej niż :value przedmiotów.',
  ),
  'max' => 
  array (
    'numeric' => 'Wartość :attribute nie może być większa niż :max.',
    'file' => 'Wartość :attribute nie może być większa niż :max kilobajty.',
    'string' => 'Wartość :attribute nie może być większa niż :max znaków.',
    'array' => ' :attribute nie może mieć więcej niż :max przedmiotów.',
  ),
  'mimes' => ' :attribute musi być plikiem typu: :values.',
  'mimetypes' => ' :attribute musi być plikiem typu: :values.',
  'min' => 
  array (
    'numeric' => 'Wartość :attribute musi wynosić co najmniej :min.',
    'file' => 'Wartość :attribute musi wynosić co najmniej :min kilobajty.',
    'string' => ' :attribute musi mieć co najmniej :min znaki.',
    'array' => ' :attribute musi mieć co najmniej :min przedmioty.',
  ),
  'not_in' => 'Wybrany :attribute jest nieprawidłowy.',
  'not_regex' => 'Format :attribute jest nieprawidłowy.',
  'numeric' => ' :attribute musi być liczbą.',
  'present' => 'Pole :attribute musi być obecne.',
  'regex' => 'Format :attribute jest nieprawidłowy.',
  'required' => 'Wymagane jest pole :attribute.',
  'required_if' => 'Pole :attribute jest wymagane, gdy :other to :value.',
  'required_unless' => 'Pole :attribute jest wymagane, chyba że :other jest w :values.',
  'required_with' => 'Pole :attribute jest wymagane, gdy obecny jest :values.',
  'required_with_all' => 'Pole :attribute jest wymagane, gdy obecne są :values.',
  'required_without' => 'Pole :attribute jest wymagane, gdy nie ma :values.',
  'required_without_all' => 'Pole :attribute jest wymagane, gdy nie ma żadnego z :values.',
  'same' => ' :attribute i :other muszą się zgadzać.',
  'size' => 
  array (
    'numeric' => ' :attribute musi być :size.',
    'file' => 'Wartość :attribute musi wynosić :size kilobajty.',
    'string' => ' :attribute musi być znakami :size.',
    'array' => ' :attribute musi zawierać :size przedmioty.',
  ),
  'starts_with' => ' :attribute musi zaczynać się od jednego z następujących: :values',
  'string' => ' :attribute musi być łańcuchem.',
  'timezone' => ' :attribute musi być prawidłową strefą.',
  'unique' => ' :attribute został już zajęty.',
  'uploaded' => 'Nie udało się przesłać :attribute.',
  'url' => 'Format :attribute jest nieprawidłowy.',
  'uuid' => ' :attribute musi być prawidłowym identyfikatorem UUID.',
  'custom' => 
  array (
    'attribute-name' => 
    array (
      'rule-name' => 'niestandardowy komunikat',
    ),
  ),
  'attributes' => 
  array (
  ),
);