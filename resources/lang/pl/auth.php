<?php 
return array (
  'failed' => 'Wprowadzono nieprawidłowe dane.',
  'throttle' => 'Zbyt wiele prób logowania. Spróbuj ponownie za kilka :seconds sekund.',
);