<?php 
return array (
  'password' => 'Hasło musi mieć co najmniej 8 znaków.',
  'reset' => 'Twoje hasło zostało zresetowane',
  'sent' => 'Wysłaliśmy Ci link do resetowania hasła na adres e-mail!',
  'token' => 'Wprowadzono niepoprawny token.',
  'user' => 'Nie możemy znaleźć użytkownika o podanym adresie e-mail.',
);