<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeroesTable extends Migration
{

    public function up()
    {
        Schema::create('heroes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('rase')->default('Człowiek');
            $table->string('proffesion')->default('Chłop');
            $table->string('sex')->default('Mężczyzna');
            $table->integer('WS')->default('0');
            $table->integer('BS')->default('0');
            $table->integer('T')->default('0');
            $table->integer('S')->default('0');
            $table->integer('Ag')->default('0');
            $table->integer('Int')->default('0');
            $table->integer('WP')->default('0');
            $table->integer('Fel')->default('0');
            $table->string('portrait')->default('default.jpg');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('heroes');
    }
}
