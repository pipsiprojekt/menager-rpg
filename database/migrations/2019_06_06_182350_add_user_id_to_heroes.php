<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToHeroes extends Migration
{

    public function up()
    {
        Schema::table('heroes', function (Blueprint $table) {
            $table->integer('user_id');
        });
    }

    public function down()
    {
        Schema::table('heroes', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
